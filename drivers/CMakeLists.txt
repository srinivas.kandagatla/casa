# SPDX-License-Identifier: BSD-3-Clause

if(CONFIG_CALCULATOR)
	add_subdirectory(calculator)
endif()
if(CONFIG_HELLO_CASA)
	add_subdirectory(hellocasa)
endif()
