# SPDX-License-Identifier: BSD-3-Clause

# Kconfig targets

include(${CMAKE_CURRENT_LIST_DIR}/defconfigs.cmake)

add_custom_target(
	menuconfig
	COMMAND ${CMAKE_COMMAND} -E env
		srctree=${PROJECT_SOURCE_DIR}
		CC_VERSION_TEXT=${CC_VERSION_TEXT}
		ARCH=${ARCH}
		CASA_VERSION=${CASA_VERSION}
		MAJOR_VERSION=${MAJOR_VERSION}
		MINOR_VERSION=${MINOR_VERSION}
		PATCHLEVEL=${PATCHLEVEL}
		${PYTHON3} ${PROJECT_SOURCE_DIR}/scripts/kconfig/menuconfig.py
		${PROJECT_SOURCE_DIR}/Kconfig
	WORKING_DIRECTORY ${GENERATED_DIRECTORY}
	VERBATIM
	USES_TERMINAL
)

add_custom_target(
	overrideconfig
	COMMAND ${CMAKE_COMMAND} -E env
		srctree=${PROJECT_SOURCE_DIR}
		CC_VERSION_TEXT=${CC_VERSION_TEXT}
		ARCH=${ARCH}
		${PYTHON3} ${PROJECT_SOURCE_DIR}/scripts/kconfig/overrideconfig.py
		${PROJECT_SOURCE_DIR}/Kconfig
		${PROJECT_BINARY_DIR}/override.config
	WORKING_DIRECTORY ${GENERATED_DIRECTORY}
	VERBATIM
	USES_TERMINAL
)

add_custom_target(genconfig DEPENDS ${CONFIG_H_PATH})

add_custom_target(
	olddefconfig
	COMMAND ${CMAKE_COMMAND} -E env
		srctree=${PROJECT_SOURCE_DIR}
		CC_VERSION_TEXT=${CC_VERSION_TEXT}
		ARCH=${ARCH}
		${PYTHON3} ${PROJECT_SOURCE_DIR}/scripts/kconfig/olddefconfig.py
		${PROJECT_SOURCE_DIR}/Kconfig
	WORKING_DIRECTORY ${GENERATED_DIRECTORY}
	VERBATIM
	USES_TERMINAL
)

add_custom_target(
	alldefconfig
	COMMAND ${CMAKE_COMMAND} -E env
		srctree=${PROJECT_SOURCE_DIR}
		CC_VERSION_TEXT=${CC_VERSION_TEXT}
		ARCH=${ARCH}
		${PYTHON3} ${PROJECT_SOURCE_DIR}/scripts/kconfig/alldefconfig.py
		${PROJECT_SOURCE_DIR}/Kconfig
	WORKING_DIRECTORY ${GENERATED_DIRECTORY}
	VERBATIM
	USES_TERMINAL
)

add_custom_target(
	savedefconfig
	COMMAND ${CMAKE_COMMAND} -E env
		srctree=${PROJECT_SOURCE_DIR}
		CC_VERSION_TEXT=${CC_VERSION_TEXT}
		ARCH=${ARCH}
		${PYTHON3} ${PROJECT_SOURCE_DIR}/scripts/kconfig/savedefconfig.py
		${PROJECT_SOURCE_DIR}/Kconfig
		${PROJECT_BINARY_DIR}/defconfig
	WORKING_DIRECTORY ${GENERATED_DIRECTORY}
	COMMENT "Saving minimal configuration to: ${PROJECT_BINARY_DIR}/defconfig"
	VERBATIM
	USES_TERMINAL
)
